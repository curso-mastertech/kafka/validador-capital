package br.com.mastertech.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ValidadorCapitalApplication {

	public static void main(String[] args) {
		SpringApplication.run(ValidadorCapitalApplication.class, args);
	}

}
