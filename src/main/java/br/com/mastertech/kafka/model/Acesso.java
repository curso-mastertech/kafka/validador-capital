package br.com.mastertech.kafka.model;

public class Acesso {

    private String cliente;
    private String porta;
    private boolean liberado;

    public Acesso() {
    }

    public Acesso(String cliente, String porta, boolean liberado) {
        this.cliente = cliente;
        this.porta = porta;
        this.liberado = liberado;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getPorta() {
        return porta;
    }

    public void setPorta(String porta) {
        this.porta = porta;
    }

    public boolean isLiberado() {
        return liberado;
    }

    public void setLiberado(boolean liberado) {
        this.liberado = liberado;
    }
}
