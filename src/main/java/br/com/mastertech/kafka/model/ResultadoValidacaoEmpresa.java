package br.com.mastertech.kafka.model;

public class ResultadoValidacaoEmpresa {

    private String cnpj;
    private String capitalSocial;
    private boolean capitalAceito;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getCapitalSocial() {
        return capitalSocial;
    }

    public void setCapitalSocial(String capitalSocial) {
        this.capitalSocial = capitalSocial;
    }

    public boolean isCapitalAceito() {
        return capitalAceito;
    }

    public void setCapitalAceito(boolean capitalAceito) {
        this.capitalAceito = capitalAceito;
    }
}
