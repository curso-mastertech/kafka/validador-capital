package br.com.mastertech.kafka.exception;

public class ConsultaCNPJException extends RuntimeException {
    public ConsultaCNPJException(String cnpj) {
        super("Erro ao buscar empresa pelo cnpj " + cnpj);
    }
}
