package br.com.mastertech.kafka.service;

import br.com.mastertech.kafka.model.ResultadoValidacaoEmpresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ResultadoValidacaoService {

    public static final String TOPICO_RESULTADO_AUDIT = "pedro-biro-2";

    @Autowired
    private KafkaTemplate<String, ResultadoValidacaoEmpresa> sender;

    public void salvaResultadoValidacao(ResultadoValidacaoEmpresa resultado) {
        sender.send(TOPICO_RESULTADO_AUDIT, "1", resultado);
    }
}
