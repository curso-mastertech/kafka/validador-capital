package br.com.mastertech.kafka.service;

import br.com.mastertech.kafka.client.ConsultaCNPJClient;
import br.com.mastertech.kafka.client.dto.EmpresaDTO;
import br.com.mastertech.kafka.exception.ConsultaCNPJException;
import br.com.mastertech.kafka.model.ResultadoValidacaoEmpresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ValidadorCapitalService {

    @Autowired
    private ConsultaCNPJClient apigovClient;

    @Autowired
    private ResultadoValidacaoService resultadoValidacaoService;

    public void valida(String cnpj) {
        EmpresaDTO empresaConsultada = apigovClient
                .findByCNPJ(cnpj)
                .filter(e -> e.getCapital_social() != null)
                .filter(e -> e.getCnpj() != null)
                .orElseThrow(() -> new ConsultaCNPJException(cnpj));

        ResultadoValidacaoEmpresa resultado = criaResultado(empresaConsultada);
        resultadoValidacaoService.salvaResultadoValidacao(resultado);
    }

    private ResultadoValidacaoEmpresa criaResultado(EmpresaDTO empresa) {
        ResultadoValidacaoEmpresa resultado = new ResultadoValidacaoEmpresa();
        resultado.setCnpj(empresa.getCnpj());
        resultado.setCapitalSocial(empresa.getCapital_social());
        resultado.setCapitalAceito(isCapitalAcimaHumMilhao(empresa.getCapital_social()));
        return resultado;
    }

    private boolean isCapitalAcimaHumMilhao(String capitalSocial) {
        return Double.parseDouble(capitalSocial) > 1000000.00;
    }
}