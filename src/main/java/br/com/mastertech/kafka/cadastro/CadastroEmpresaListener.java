package br.com.mastertech.kafka.cadastro;

import br.com.mastertech.kafka.model.Empresa;
import br.com.mastertech.kafka.service.ValidadorCapitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class CadastroEmpresaListener {

    public static final String TOPICO_REGISTRO_CNPJ = "pedro-biro-1";

    @Autowired
    private ValidadorCapitalService validadorCapitalService;

    @KafkaListener(topics = TOPICO_REGISTRO_CNPJ, groupId = "grupo1")
    public void recebeCadastroEmpresa(@Payload Empresa empresa) {
        validadorCapitalService.valida(empresa.getCnpj());
    }

}
